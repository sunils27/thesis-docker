# README #
** FOR THE MOMENT ALL THE SERVICES ARE IN THE SAME CONTAINER, SO THEY HAVE TO BE STARTED UP MANUALLY. THIS IS GOING TO CHANGE IN THE NEAR FUTURE. THE FOLLOWING INSTRUCTIONS ARE FOR BUILDING AND RUNNING THIS CONTAINER. **

** GETTING THE CONTAINER **

  1.Use git to get the Dockerfile and associated files. Make sure they are all in the same directory. Say, it has been cloned into a folder /home/sunils/thesis-docker. 
```
$ git clone https://sunils27@bitbucket.org/sunils27/thesis-docker.git
```

** BUILDING THE CONTAINER **

1.Run the Docker build command. 
`Example:` 
```
$ docker build -t sunils_thesis_work ./thesis-docker/ 
```
This should build the container successfully in a few minutes. 
*** You may have to do a sudo (or equivalent) depending on your user privileges. ***

** RUNNING THE CONTAINER **

1.Run the container.  `-p 8080:8080` maps the port `8080` of the container to your host
port `8080` so you can access it via `{hostip}:8080`! If port `8080` is in use on your host
already, change this mapping, ie `-p {openport on host}:8080`
`Example:`
```
$ docker run -t -i -p 8080:8080 sunils_thesis_work /bin/bash
```
This will run the container and you will be presented with a container bash shell.

** STARTING THE SERVICES **

1.Start basic services. Starting tomcat should automatically deploy the FMSwebservice endpoint.
```
# service tomcat7 start
# service rabbitmq-server start
```
2.Start the FMS.
```
# cd /home/FMS
# nohup java -jar FMSbin-0.2.jar &
```
3.Start the solver. 
```
# cd /home/stovemodel
# nohup stoverunner &
```

*** I recommend running the stoverunner in supervised mode. In which case: ***
```
# cd /home/stovemodel/
# mv stoverunner run
# cd ../
# supervise ./stovemodel &
```


** MAKING SURE IT WORKS **

Get the docker container IP address by running the following commands on your container **HOST** machine. 
```
$ docker ps
```
Find your container ID
```
$ docker inspect <containerID> | grep "IPAddress"
```
Find the IP address in the output, say, 172.17.0.81
Open up a browser and go to
```
http://172.17.0.81:8080/FMSwebservice/rest/fms/getlogs
```
You should see a message that says, there are no logs available. 

Note if you've mapped the ports when running the container with `-p {hostport}:8080` you should be able to execute
```
http://localhost:{hostport}/FMSwebservice/rest/fms/getlogs
```

To POST a message, use the chrome advanced REST client ( or something similar, curl etc. ). Make a POST request at this API endpoint
```
http://172.17.0.81:8080/FMSwebservice/rest/fms/post
```
or if you've mapped the ports of your container
```
http://localhost:{hostport}/FMSwebservice/rest/fms/post
```
with the payload and "content-type" "application/json". 
```
{"solverlist":"stovemodel", "Dc":0.1, "Hc":0.2326, "gc":0.025, "Hp":0.11052753, "Ds":0.101, "ks":1.0, "Hsh":0.08, "Tsh":0.0005, "ksh":35.0, "gp":0.01, "gs":0.008, "Dp":0.24, "eff":-1 , "fmshost":"localhost" }
```


You should get back a response that looks like this
```
{"solverlist":"stovemodel,b7a19334-902a-4972-98f8-c67fca7c2765","Dc":0.1,"Hc":0.2326,"gc":0.025,"Hp":0.11052753,"Ds":0.101,"ks":1,"Hsh":0.08,"Tsh":5.0E-4,"ksh":35,"gp":0.01,"gs":0.008,"Dp":0.24,"eff":0.3473375141620636,"fmshost":"54.174.254.203","index":1}
```

** IMPORTANT NOTE: **


The tomcat7 and rabbitmq-server are installed in default mode with default security settings. If you wish to secure them please refer to the appropriate documentation. 

### Who do I talk to? ###
```
* Sunil Suram (suram.sunil@gmail.com)
```