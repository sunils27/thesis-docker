#Dockerfile for creating a container for dissertation work
FROM ubuntu:14.04
MAINTAINER Sunil Suram (suram.sunil@gmail.com)

#install basic stuff
RUN apt-get update
RUN apt-get install openjdk-7-jdk --yes

#install tomcat
RUN apt-get install tomcat7 tomcat7-docs tomcat7-admin tomcat7-examples --yes
#install RabbitMQ
RUN apt-get install rabbitmq-server --yes
RUN apt-get install librabbitmq-dev --yes

#install some useful utils
RUN apt-get install htop --yes
RUN apt-get install vim --yes
RUN apt-get install daemontools --yes

#boost libs
RUN apt-get install libboost-all-dev --yes

#add files
ADD /FMSwebservice/ /var/lib/tomcat7/webapps/
ADD http://ssuram.s3-website-us-east-1.amazonaws.com/FMS/FMSbin-0.2.jar /home/FMS/
ADD http://ssuram.s3-website-us-east-1.amazonaws.com/FMS/rmq.prop /home/FMS/
ADD http://ssuram.s3-website-us-east-1.amazonaws.com/FMS/solverreg.prop /home/FMS/
ADD http://ssuram.s3-website-us-east-1.amazonaws.com/stovemodel/stoverunner /home/stovemodel/
ADD http://ssuram.s3-website-us-east-1.amazonaws.com/stovemodellibs/libSimpleAmqpClient.so.2 /usr/lib/
ADD http://ssuram.s3-website-us-east-1.amazonaws.com/stovemodellibs/librabbitmq.so /usr/lib/
ADD http://ssuram.s3-website-us-east-1.amazonaws.com/stovemodellibs/librabbitmq.so.4 /usr/lib/

WORKDIR /home/stovemodel
RUN ["chmod", "777", "stoverunner"]
WORKDIR /home

#expose ports
EXPOSE 8080
